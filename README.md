# h5p-ImageSequencing
A free HTML5 based image sequencing content type allowing authors to add a sequence of their own images ( and optional image description) to the  game in proper order.Players of the game will be presented with randomly shuffled list of sequence images. They have to place each image by their correct order in the sequence by drag and drop.
<br/><a href="https://h5p.org"> See it in action on h5p.org </a><br/>

## Screenshots

<img src="https://gitlab.com/icfoss/H5P-ImageSequencing/raw/master/screenshots/Screenshot_2019-11-29%20Image%20Sequencing.png">



## License

[GPL v3](LICENSE)

## Credits

This content type is developed under the Media & Learning Lab of [ICFOSS](https://icfoss.in)
